package com.velldrin.smartvoiceassistant;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class NotificationButtonReceiver extends BroadcastReceiver{
	
	private static final String KEY_BUTTON_ONE = "com.velldrin.smartvoiceassistant.buttonone";
	private static final String KEY_BUTTON_TWO = "com.velldrin.smartvoiceassistant.buttontwo";
	private static final String KEY_BUTTON_THREE = "com.velldrin.smartvoiceassistant.buttonthree";
	


	@Override
	public void onReceive(Context context, Intent intent) {
		
		  if(intent.getAction().equals(KEY_BUTTON_ONE))
		  {
			  Intent it = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
			  context.sendBroadcast(it);

			  context.startService(new Intent(context, VoiceService.class));

			
		  }
		  else if(intent.getAction().equals(KEY_BUTTON_TWO))
		  {
			  Intent it = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
			  context.sendBroadcast(it);
			  
			  context.stopService(new Intent(context, VoiceService.class));
			  
		  }
		  else if(intent.getAction().equals(KEY_BUTTON_THREE))
		  {
			  Intent it = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
			  context.sendBroadcast(it);
			  
			  Intent keyIntent = new Intent(context, SettingsActivity.class);
			  keyIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			  context.startActivity(keyIntent);
		  }
		  
		
	}

}
