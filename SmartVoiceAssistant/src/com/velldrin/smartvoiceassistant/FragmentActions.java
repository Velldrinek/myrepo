package com.velldrin.smartvoiceassistant;


import java.util.ArrayList;

import com.actionbarsherlock.app.SherlockFragment;
import com.velldrin.smartvoiceassistant.R.id;

import android.os.Bundle;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class FragmentActions extends SherlockFragment implements OnClickListener {

	 private Button contactsBtn,
	 				navigationBtn,
	 				searchBtn,
	 				appBtn,
	 				enableBtn,
	 				disableBtn;
	 

	 public static Fragment newInstance(Context context) {
	        FragmentActions f = new FragmentActions();
	 
	        return f;
	    }
	 
	    @Override
	    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
	        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_actions, null);
	        
	       if (container == null) {
	            
	            return null;
	        }
	       
	       contactsBtn = (Button) root.findViewById(R.id.phone_btn);
	       navigationBtn = (Button) root.findViewById(R.id.navigation_btn);
	       searchBtn = (Button) root.findViewById(R.id.search_btn);
	       appBtn = (Button) root.findViewById(R.id.app_btn);
	       enableBtn = (Button) root.findViewById(R.id.enable_btn);
	       disableBtn = (Button) root.findViewById(R.id.disable_btn);
	       
	       contactsBtn.setOnClickListener(this);
	       navigationBtn.setOnClickListener(this);
	       searchBtn.setOnClickListener(this);
	       appBtn.setOnClickListener(this);
	       enableBtn.setOnClickListener(this);
	       disableBtn.setOnClickListener(this);
	       
	       return root;
	    }

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			FragmentTransaction tx;
			switch (v.getId())
			{
			case R.id.phone_btn:
				
			  tx = getActivity().getSupportFragmentManager().beginTransaction();
	          tx.replace(R.id.main, new FragmentActionsContacts()).addToBackStack( "back" ).commit();
			
			break;
			case R.id.navigation_btn:
				
			  tx = getActivity().getSupportFragmentManager().beginTransaction();
		      tx.replace(R.id.main, new FragmentActionsNavigation()).addToBackStack( "back" ).commit();
		         
			break;
				
			case R.id.search_btn:
			  tx = getActivity().getSupportFragmentManager().beginTransaction();
			  tx.replace(R.id.main, new FragmentActionsSearch()).addToBackStack( "back" ).commit();
			break;
				
			case R.id.app_btn:
			  tx = getActivity().getSupportFragmentManager().beginTransaction();
			  tx.replace(R.id.main, new FragmentActionsApp()).addToBackStack( "back" ).commit();
			break;
			case R.id.enable_btn:				
				getActivity().startService(new Intent(getActivity(),VoiceService.class));				
			break;
			case R.id.disable_btn:
				getActivity().stopService(new Intent(getActivity(),VoiceService.class));
			break;
				
			}
		}

}
