package com.velldrin.smartvoiceassistant;

import java.util.ArrayList;

import com.actionbarsherlock.app.SherlockActivity;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class SmsActivity extends SherlockActivity {
	
	 public static final String SMS_SEND_ACTION = "CTS_SMS_SEND_ACTION";
	 public static final String SMS_DELIVERY_ACTION = "CTS_SMS_DELIVERY_ACTION";
	
	private ScrollView smsView;
	private TextView contact;
	private TextView number;
	
	private EditText smsText;
	
	private Context context;
	
	private final BroadcastReceiver mReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			
			if(intent.getBooleanExtra("deleteAllSMS", false))
			{
				deleteAll();
			}
			else if(intent.getBooleanExtra("deleteSMS", false))
			{
				deleteWord();
			}
			else if(intent.getBooleanExtra("sendSMS", false))
			{
				sendSMS();
				finish();
			}	
			else if(intent.getBooleanExtra("dismissSMS", false))
			{
				finish();
			}
			else
			{
				addText(intent.getStringExtra("addText"));
			}
			
				
		}
		};
		
		
		
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sms);

		String contactS = getIntent().getStringExtra("Contact");
		String numberS = getIntent().getStringExtra("Number");
		
		contact = (TextView) findViewById(R.id.ContactId);
		number = (TextView) findViewById(R.id.NumberId);
		smsText = (EditText) findViewById(R.id.smsText);
		
		contact.setText(contactS);
		number.setText(numberS);
		
		smsView = (ScrollView) findViewById(R.id.scrollView);
		
		context = this;
		
		
		

			
    
	}
	
	@Override
	public void onResume()
	{
		super.onResume();
		VoiceService.mode=1;
		 
		registerReceiver(mReceiver, new IntentFilter("SMS"));
	}
	@Override
	public void onPause()
	{
		super.onPause();
		VoiceService.mode=0;
		
		unregisterReceiver(mReceiver);
		finish();
		
	}
	
	public void addText(String text)
	{
		if(smsText.getText().toString().equals(""))
		{
			smsText.setText(text);
		}
		else smsText.setText(smsText.getText().toString() + " " + text);
		
		smsView.post(new Runnable(){
			@Override 
			public void run()
			{
				smsView.fullScroll(View.FOCUS_DOWN);
			}
		});
	}
	
	public void deleteWord()
	{
		// do poprawy
		String message = smsText.getText().toString();

		if(message.length()!=0)
		{
			while(message.length()!=0 && !message.endsWith(" "))
			{
				message = message.substring(0,message.length()-1);
				if(message.endsWith(" "))
				{
					message = message.substring(0,message.length()-1);
					break;
				}
			}
		}
		
		smsText.setText(message);
		
		smsView.post(new Runnable(){
			@Override 
			public void run()
			{
				smsView.fullScroll(View.FOCUS_DOWN);
			}
		});
	}

	public void deleteAll()
	{
		smsText.setText("");
	}
	
	private void sendSMS()
	{
		try
		{
			//pewnie mozna przechwycic by sprawdzic czy wiadomosc wyslano
			Intent mSendIntent = new Intent(SMS_SEND_ACTION);
			Intent mDeliveryIntent = new Intent(SMS_DELIVERY_ACTION);
		        
			SmsManager sm = SmsManager.getDefault();
			ArrayList<String> parts =sm.divideMessage(smsText.getText().toString());
			int numParts = parts.size();

			ArrayList<PendingIntent> sentIntents = new ArrayList<PendingIntent>();
			ArrayList<PendingIntent> deliveryIntents = new ArrayList<PendingIntent>();

			sentIntents.add(PendingIntent.getBroadcast(context, 0, mSendIntent, 0));
			deliveryIntents.add(PendingIntent.getBroadcast(context, 0, mDeliveryIntent, 0));
			sm.sendMultipartTextMessage(number.getText().toString(),null, parts, sentIntents, deliveryIntents);
			
			
		
		}
		catch(Exception e)
		{
			Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
		}
	}
	

}