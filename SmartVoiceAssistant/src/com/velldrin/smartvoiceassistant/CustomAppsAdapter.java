package com.velldrin.smartvoiceassistant;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomAppsAdapter extends ArrayAdapter<App> {
	  private final Context context;
	  private App[] app;
	  
	  public CustomAppsAdapter(Context context, ArrayList<App> app) {
	    super(context, R.layout.row, app);
	    this.context = context;
	    
	    this.app = new App[app.size()];
	    this.app = app.toArray(this.app);

	  }

	  @Override
	  public View getView(int position, View convertView, ViewGroup parent) {
	    LayoutInflater inflater = (LayoutInflater) context
	        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    View rowView = inflater.inflate(R.layout.row2, parent, false);
	    
	    TextView nameTxt = (TextView) rowView.findViewById(R.id.name_app);
	    ImageView iconApp = (ImageView) rowView.findViewById(R.id.icon_app);
	    TextView keyTxt = (TextView) rowView.findViewById(R.id.key_app);
	    
	    nameTxt.setText(this.app[position].getAppName());
	    iconApp.setImageDrawable(this.app[position].getAppIcon());
	    keyTxt.setText(this.app[position].getAppKey());
	    
	    return rowView;
	  }

}
