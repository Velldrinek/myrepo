package com.velldrin.smartvoiceassistant;

import java.util.ArrayList;
import com.actionbarsherlock.app.SherlockFragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.speech.RecognizerIntent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import android.provider.ContactsContract;
import android.database.Cursor;

public class FragmentActionsContacts extends SherlockFragment implements OnClickListener{
	
	private static final int VOICE_RECOGNITION_PHONE_CODE = 6661;
	private static final int VOICE_RECOGNITION_WRITE_CODE = 6662;
	private static final int VOICE_RECOGNITION_DELETE_CODE = 6663;
	private static final int VOICE_RECOGNITION_DELETE_ALL_CODE = 6664;
	private static final int VOICE_RECOGNITION_SEND_CODE = 6665;
	private static final int VOICE_RECOGNITION_DISMISS_CODE = 6666;
	private static final int VOICE_RECOGNITION_DISMISS_GET_CODE = 6667;
	private static final int VOICE_RECOGNITION_READ_GET_CODE = 6668;
	
	private Button phoneBtn,
				   smsWriteBtn,
				   smsDeleteBtn,
				   smsDeleteAllBtn,
				   smsSendBtn,
				   smsDismissBtn,
				   smsDismissGetBtn,
				   smsReadGetBtn,
				   
				   confirmBtn,
				   cancelBtn;
	
	private ListView lv;
	
	private static boolean state = false;
	
	private ProgressDialog mProgressDialog;
	
	private ArrayList<Contact> contactList;
	
	 public static Fragment newInstance(Context context) {
		 FragmentActionsContacts f = new FragmentActionsContacts();
		 
	        return f;
	    }
	 
	    @Override
	    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
	        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_actions_contacts, null);
	        
	       if (container == null) {
	            
	            return null;
	        }
	       
	       lv = (ListView) root.findViewById(R.id.contact_list);
	       setContactList();
	       
	       phoneBtn = (Button) root.findViewById(R.id.phone_btn);
	       smsWriteBtn = (Button) root.findViewById(R.id.writesms_btn);
	       smsDeleteBtn = (Button) root.findViewById(R.id.deletesms_btn);
	       smsDeleteAllBtn = (Button) root.findViewById(R.id.deletesmsall_btn);
	       smsSendBtn = (Button) root.findViewById(R.id.sendsms_btn);
	       smsDismissBtn = (Button) root.findViewById(R.id.dismiss_btn);
	       smsDismissGetBtn = (Button) root.findViewById(R.id.dismiss_get_btn);
	       smsReadGetBtn = (Button) root.findViewById(R.id.read_get_btn);
	       confirmBtn = (Button) root.findViewById(R.id.confirm_btn);
	       cancelBtn = (Button) root.findViewById(R.id.cancel_btn);
	       
	       phoneBtn.setOnClickListener(this);
	       smsWriteBtn.setOnClickListener(this);
	       smsDeleteBtn.setOnClickListener(this);
	       smsDeleteAllBtn.setOnClickListener(this);
	       smsSendBtn.setOnClickListener(this);
	       smsDismissBtn.setOnClickListener(this);
	       smsDismissGetBtn.setOnClickListener(this);
	       smsReadGetBtn.setOnClickListener(this);
	       confirmBtn.setOnClickListener(this);
	       cancelBtn.setOnClickListener(this);
	       
	       ActionDbHandler dbhandler = new ActionDbHandler(getActivity());
	       
	       if(!dbhandler.getAction("phone").getCommandS().equals("###"))
		   {
		       phoneBtn.setText(dbhandler.getAction("phone").getCommandS());
		   }
	       if(!dbhandler.getAction("writeSMS").getCommandS().equals("###"))
		   {
		       smsWriteBtn.setText(dbhandler.getAction("writeSMS").getCommandS());
		   }
	       if(!dbhandler.getAction("deleteSMS").getCommandS().equals("###"))
		   {
		       smsDeleteBtn.setText(dbhandler.getAction("deleteSMS").getCommandS());
		   }
	       if(!dbhandler.getAction("deleteAllSMS").getCommandS().equals("###"))
		   {
		       smsDeleteAllBtn.setText(dbhandler.getAction("deleteAllSMS").getCommandS());
		   }
	       if(!dbhandler.getAction("sendSMS").getCommandS().equals("###"))
		   {
		       smsSendBtn.setText(dbhandler.getAction("sendSMS").getCommandS());
		   }
	       if(!dbhandler.getAction("dismissSMS").getCommandS().equals("###"))
		   {
		       smsDismissBtn.setText(dbhandler.getAction("dismissSMS").getCommandS());
		   }
	       if(!dbhandler.getAction("dismissGetSMS").getCommandS().equals("###"))
		   {
		       smsDismissGetBtn.setText(dbhandler.getAction("dismissGetSMS").getCommandS());
		   }
	       if(!dbhandler.getAction("readGetSMS").getCommandS().equals("###"))
		   {
		       smsReadGetBtn.setText(dbhandler.getAction("readGetSMS").getCommandS());
		   }
		       
	       dbhandler.close();
			 	
			 
		       
	       
	       return root;
	    }

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Fragment f = Fragment.instantiate(getActivity(), "com.velldrin.smartvoiceassistant.FragmentActions");
			FragmentTransaction tx = getActivity().getSupportFragmentManager().beginTransaction();
			
			switch(v.getId())
			{
			case R.id.phone_btn:	
				startVoiceRecognitionActivity(VOICE_RECOGNITION_PHONE_CODE);
			break;
			case R.id.writesms_btn:
				startVoiceRecognitionActivity(VOICE_RECOGNITION_WRITE_CODE);
			break;
			case R.id.deletesms_btn:
				startVoiceRecognitionActivity(VOICE_RECOGNITION_DELETE_CODE);
			break;
			case R.id.deletesmsall_btn:
				startVoiceRecognitionActivity(VOICE_RECOGNITION_DELETE_ALL_CODE);
			break;
			case R.id.dismiss_btn:
				startVoiceRecognitionActivity(VOICE_RECOGNITION_DISMISS_CODE);
			break;
			case R.id.dismiss_get_btn:
				startVoiceRecognitionActivity(VOICE_RECOGNITION_DISMISS_GET_CODE);
			break;
			case R.id.read_get_btn:
				startVoiceRecognitionActivity(VOICE_RECOGNITION_READ_GET_CODE);
			break;
			case R.id.sendsms_btn:
				startVoiceRecognitionActivity(VOICE_RECOGNITION_SEND_CODE);
			break;
			case R.id.confirm_btn:
				confirmChanges();
			break;		
			case R.id.cancel_btn:
				tx.replace(R.id.main, f);
		        tx.commit();
			break;
			}
			
		}
		
		
		private void startVoiceRecognitionActivity(int code) {
			    
				if(VoiceService.status==1)
					{
						getActivity().stopService(new Intent(getActivity(), VoiceService.class));
						state=true;
					}
				
				        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
			
				        // Specify the calling package to identify your application
				        intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, getClass().getPackage().getName());
			 
				        // Display an hint to the user about what he should say.
				        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, getActivity().getResources().getString(R.string.dialog_message_recognition));
			
				        // Given an hint to the recognizer about what the user is going to say
				        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
				                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
			
				        // Specify how many results you want to receive. The results will be sorted
				        // where the first result is the one with higher confidence.
				        intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1);
				        startActivityForResult(intent, code);
				        
				        
				        
				    }
		
		@Override
		 public void onActivityResult(int requestCode, int resultCode, Intent data) {
			 
			   
		            super.onActivityResult( requestCode, resultCode, data );
		            		        
		            
		            if(data!=null)
		            {
			            ArrayList<String> matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
			            String word = matches.get(0);          
			           
			            if(requestCode == VOICE_RECOGNITION_PHONE_CODE) {
				            
				            phoneBtn.setText(word);
				            
				        }
				          
				        else if (requestCode == VOICE_RECOGNITION_WRITE_CODE) {
	
				        	smsWriteBtn.setText(word);
				            
				        }		 	        
				        else if (requestCode == VOICE_RECOGNITION_DELETE_CODE) {
				        	
				        	smsDeleteBtn.setText(word);
				        	
				        }
				        else if (requestCode == VOICE_RECOGNITION_DELETE_ALL_CODE) {
				            
				        	smsDeleteAllBtn.setText(word);
				        	
				        }
				        else if (requestCode == VOICE_RECOGNITION_SEND_CODE) {
				        	
				        	smsSendBtn.setText(word);

				        }
				        else if (requestCode == VOICE_RECOGNITION_DISMISS_CODE) {
				        	
				        	smsDismissBtn.setText(word);
				        	
				        }
				        else if (requestCode == VOICE_RECOGNITION_DISMISS_GET_CODE) {
				        	
				        	smsDismissGetBtn.setText(word);
				        	
				        }
				        else if (requestCode == VOICE_RECOGNITION_READ_GET_CODE) {
				        	
				        	smsReadGetBtn.setText(word);
				        	
				        }
				        else
				        {
				        	//zabezpieczenie przed podwojeniem
				        	for(Contact contact : contactList)
				        	{
				        		if(contact.getKey().equals(word))
				        		{
				        			word = contactList.get(requestCode).getKey();
				        			Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.toast_message_in_use), Toast.LENGTH_SHORT).show();
				        		}
				        	}
				        	contactList.get(requestCode).setKey(word);
				        	setListView(contactList);
				        }
		            }

		            
					if(state==true)
					{
						getActivity().startService(new Intent(getActivity(), VoiceService.class));
						state=false;
					}
		 	    }

		
		public void setContactList()
		{
			 mProgressDialog = new ProgressDialog(getActivity());
			 mProgressDialog.setCancelable(false);
			 mProgressDialog.setCanceledOnTouchOutside(false);
			 mProgressDialog.setTitle(getActivity().getResources().getString(R.string.fragment_dialog_loading_title));
			 mProgressDialog.setMessage(getActivity().getResources().getString(R.string.fragment_dialog_loading_contacts));
	     	 mProgressDialog.setIndeterminate(false);
	     	 mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			
			new AsyncTask<Void, Void, Void>(){
	            @Override
	            protected void onPreExecute(){
	                mProgressDialog.show();
	            }
	            
		        @Override
		        protected Void doInBackground(Void... arg0) {
		        	
		        	doContactList();
		     	   
		            return null;
		        }
		           @Override
		            protected void onPostExecute(Void result) {
		        	   
		        	    setListView(contactList);
		        	   try{
		        	    mProgressDialog.dismiss();
		        	   }
		        	   catch(Exception e)
		        	   {
		        		   
		        	   }
		        }
		    }.execute();
			
			 	
		}
		
		private void doContactList()
		{
			
			try{
        		contactList = new ArrayList<Contact>();
	        	Cursor cursor = getActivity().getContentResolver().query(ContactsContract.Contacts.CONTENT_URI,null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME );
	   			    while (cursor.moveToNext()) {
	   			        String contactId = cursor.getString(cursor.getColumnIndex(
	   			                ContactsContract.Contacts._ID));
	   			        
	   			        Cursor phones = getActivity().getContentResolver().query( ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = "+ contactId, null, null);
	   			        while (phones.moveToNext()) {
	   			            
	   			            String phoneNumber = phones.getString(phones.getColumnIndex( ContactsContract.CommonDataKinds.Phone.NUMBER));
	   			            String phoneName = phones.getString(phones.getColumnIndex( ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME_PRIMARY));
	   			            
	   			            ActionDbHandler dbhandler = new ActionDbHandler(getActivity());
	   			            Action action1 = dbhandler.getAction("phone", phoneNumber);
	   			            Action action2 = dbhandler.getAction("writeSMS", phoneNumber);
	   			            dbhandler.close();
	   			            
	   			            if(action1.getCommand().equals("###") && action2.getCommand().equals("###"))
	   			            {
	   			            		contactList.add(new Contact(phoneName,phoneNumber, phoneName.toLowerCase())); 	

	   			            }
	   			            else
	   			            {
	   			            	if(action1.getWhatS().equals(action2.getWhatS()))
	   			            	{
		   			            	contactList.add(new Contact(phoneName,phoneNumber, action1.getWhatS()));	   			            	
	   			            	}
	   			            }
	   			            
	   			            
	   			            
	   			        }
	   			        phones.close();

	   			    }
	   			    cursor.close();	
	   		
        	}
        	catch(Exception e)
        	{
        		
        	}
		}
		
		private void setListView(ArrayList<Contact> contactList)
		{
			try{
				CustomContactsAdapter adapter = new CustomContactsAdapter(getActivity(), contactList);
				lv.setAdapter(adapter);
				
				lv.setOnItemClickListener(new OnItemClickListener() {
			        @Override
					public void onItemClick(AdapterView<?> parent, View view,
			                int position, long id) {
			
			        	startVoiceRecognitionActivity(position);
			     
			        }
			        
			   
			    });
				
				
				int height=0;
				for (int i = 0; i < lv.getAdapter().getCount(); i++) {
				          View listItem = lv.getAdapter().getView(i, null, lv);
				          listItem.measure(0, 0);
				          
				          if(i<2) height += listItem.getMeasuredHeight();
				          height += listItem.getMeasuredHeight();
				}
				
				ViewGroup.LayoutParams parameters = lv.getLayoutParams();
				parameters.height = height;
				lv.setLayoutParams(parameters);
			}
			catch(Exception e)
			{
				
			}
         
		}
		
		public void confirmChanges()
		{
			 mProgressDialog = new ProgressDialog(getActivity());
			 mProgressDialog.setCancelable(false);
			 mProgressDialog.setCanceledOnTouchOutside(false);
			 mProgressDialog.setTitle(getActivity().getResources().getString(R.string.fragment_dialog_saving_title));
			 mProgressDialog.setMessage(getActivity().getResources().getString(R.string.fragment_dialog_saving_text));
	     	 mProgressDialog.setIndeterminate(false);
	     	 mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			
			new AsyncTask<Void, Void, Void>(){
	            @Override
	            protected void onPreExecute(){
	                mProgressDialog.show();
	            }
	            
		        @Override
		        protected Void doInBackground(Void... arg0) {
		        	
		        	Action action;
					ActionDbHandler dbhandler = new ActionDbHandler(getActivity());
					dbhandler.deleteAction("phone");
					dbhandler.deleteAction("writeSMS");
					
					for(Contact contact : contactList)
					{
						action = new Action("phone",phoneBtn.getText().toString(),contact.getNumber(), contact.getKey());
			            
			            dbhandler.addAction(action);   
			            
						action = new Action("writeSMS",smsWriteBtn.getText().toString(),contact.getNumber(), contact.getKey());
			            
			            dbhandler.addAction(action);   	     
			            
					}
					
					
					        
		            
		            action = new Action("deleteSMS",smsDeleteBtn.getText().toString(),"#SMS#","#SMS#");
		            dbhandler.deleteAction("deleteSMS");
		            dbhandler.addAction(action);
		            
		            action = new Action("deleteAllSMS",smsDeleteAllBtn.getText().toString(),"#SMS#","#SMS#");
		            dbhandler.deleteAction("deleteAllSMS");
		            dbhandler.addAction(action);
		            
		            action = new Action("sendSMS",smsSendBtn.getText().toString(),"#SMS#","#SMS#");
		            dbhandler.deleteAction("sendSMS");
		            dbhandler.addAction(action);
		            
		            action = new Action("dismissSMS",smsDismissBtn.getText().toString(),"#SMS#","#SMS#");
		            dbhandler.deleteAction("dismissSMS");
		            dbhandler.addAction(action);
		            
		            action = new Action("dismissGetSMS",smsDismissGetBtn.getText().toString(),"#SMS#","#SMS#");
		            dbhandler.deleteAction("dismissGetSMS");
		            dbhandler.addAction(action);
		           
		            action = new Action("readGetSMS",smsReadGetBtn.getText().toString(),"#SMS#","#SMS#");
		            dbhandler.deleteAction("readGetSMS");
		            dbhandler.addAction(action);
		            
		            dbhandler.close();
		     	   
		            return null;
		        }
		           @Override
		            protected void onPostExecute(Void result) {
		        	   
		        	   try{
			        	    mProgressDialog.dismiss();
			        	    
			    			Fragment f = Fragment.instantiate(getActivity(), "com.velldrin.smartvoiceassistant.FragmentActions");
			    			FragmentTransaction tx = getActivity().getSupportFragmentManager().beginTransaction();
			    			
			    			tx.replace(R.id.main, f);
			    	        tx.commit();
		        	   }
		        	   catch(Exception e)
		        	   {
		        		   
		        	   }
		        	 }
		    }.execute();
			
			 	
		}
}
