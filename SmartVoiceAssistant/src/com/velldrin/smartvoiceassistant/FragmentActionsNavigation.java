package com.velldrin.smartvoiceassistant;

import java.util.ArrayList;
import com.actionbarsherlock.app.SherlockFragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.speech.RecognizerIntent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

public class FragmentActionsNavigation extends SherlockFragment implements OnClickListener{
	
	private static final int VOICE_RECOGNITION_NAVIGATION_CODE = 7661;
	
	private Button navigationBtn,
				   
				   confirmBtn,
				   cancelBtn;
	
	
	private static boolean state = false;
	
	private ProgressDialog mProgressDialog;
	
	
	 public static Fragment newInstance(Context context) {
		 FragmentActionsNavigation f = new FragmentActionsNavigation();
		 
	        return f;
	    }
	 
	    @Override
	    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
	        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_actions_navigation, null);
	        
	       if (container == null) {
	            
	            return null;
	        }
	       
	      
	       navigationBtn = (Button) root.findViewById(R.id.navigation_btn);
	       confirmBtn = (Button) root.findViewById(R.id.confirm_btn);
	       cancelBtn = (Button) root.findViewById(R.id.cancel_btn);
	       
	       navigationBtn.setOnClickListener(this);
	       confirmBtn.setOnClickListener(this);
	       cancelBtn.setOnClickListener(this);
	       
	       ActionDbHandler dbhandler = new ActionDbHandler(getActivity());
	       if(!dbhandler.getAction("navigation").getCommandS().equals("###"))
		   {
	       navigationBtn.setText(dbhandler.getAction("navigation").getCommandS());
		   }
	       dbhandler.close();
			
	       return root;
	    }

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Fragment f = Fragment.instantiate(getActivity(), "com.velldrin.smartvoiceassistant.FragmentActions");
			FragmentTransaction tx = getActivity().getSupportFragmentManager().beginTransaction();
			
			switch(v.getId())
			{
			case R.id.navigation_btn:	
				startVoiceRecognitionActivity(VOICE_RECOGNITION_NAVIGATION_CODE);
			break;
			case R.id.confirm_btn:
				confirmChanges();
			break;		
			case R.id.cancel_btn:
				tx.replace(R.id.main, f);
		        tx.commit();
			break;
			}
			
		}
		
		
		private void startVoiceRecognitionActivity(int code) {
			    
				if(VoiceService.status==1)
					{
						getActivity().stopService(new Intent(getActivity(), VoiceService.class));
						state=true;
					}
				
				        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
			
				        // Specify the calling package to identify your application
				        intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, getClass().getPackage().getName());
			 
				        // Display an hint to the user about what he should say.
				        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,getActivity().getResources().getString(R.string.dialog_message_recognition));
			
				        // Given an hint to the recognizer about what the user is going to say
				        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
				                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
			
				        // Specify how many results you want to receive. The results will be sorted
				        // where the first result is the one with higher confidence.
				        intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1);
				        startActivityForResult(intent, code);
			        
				    }
		
		@Override
		 public void onActivityResult(int requestCode, int resultCode, Intent data) {
			 
			   
		            super.onActivityResult( requestCode, resultCode, data );
		            		        
		            
		            if(data!=null)
		            {
			            ArrayList<String> matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
			            String word = matches.get(0);          
			           
			            if(requestCode == VOICE_RECOGNITION_NAVIGATION_CODE) {
				            
				            navigationBtn.setText(word);
				            
				        }
				          
				        
		            }
        
					if(state==true)
					{
						getActivity().startService(new Intent(getActivity(), VoiceService.class));
						state=false;
					}
		 	    }

		
		
		
		public void confirmChanges()
		{
			 mProgressDialog = new ProgressDialog(getActivity());
			 mProgressDialog.setCancelable(false);
			 mProgressDialog.setCanceledOnTouchOutside(false);
			 mProgressDialog.setTitle(getActivity().getResources().getString(R.string.fragment_dialog_saving_title));
			 mProgressDialog.setMessage(getActivity().getResources().getString(R.string.fragment_dialog_saving_text));
	     	 mProgressDialog.setIndeterminate(false);
	     	 mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			
			new AsyncTask<Void, Void, Void>(){
	            @Override
	            protected void onPreExecute(){
	                mProgressDialog.show();
	            }
	            
		        @Override
		        protected Void doInBackground(Void... arg0) {
		        	
		        	ActionDbHandler dbhandler = new ActionDbHandler(getActivity());
					dbhandler.deleteAction("navigation");
					
					Action action = new Action("navigation",navigationBtn.getText().toString(),"#NAVI#","#NAVI#");
			        dbhandler.addAction(action);   
			        
		            dbhandler.close();
		     	   
		            return null;
		        }
		           @Override
		            protected void onPostExecute(Void result) {
		        	   
		        	   try{
			        	    mProgressDialog.dismiss();
			        	    
			    			Fragment f = Fragment.instantiate(getActivity(), "com.velldrin.smartvoiceassistant.FragmentActions");
			    			FragmentTransaction tx = getActivity().getSupportFragmentManager().beginTransaction();
			    			
			    			tx.replace(R.id.main, f);
			    	        tx.commit();
		        	   }
		        	   catch(Exception e)
		        	   {
		        		   
		        	   }
		        	 }
		    }.execute();
			
			 	
		}
}
