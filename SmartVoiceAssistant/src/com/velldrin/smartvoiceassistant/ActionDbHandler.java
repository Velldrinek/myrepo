package com.velldrin.smartvoiceassistant;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ActionDbHandler extends SQLiteOpenHelper {
	
	private static final String DBNAME = "Actions.db";
	private static final int DBVERSION = 1;
	
	private static final String TABLENAME = "action_table";
	private static final String ID = "_id";
	private static final String COMMAND = "command";
	private static final String COMMAND_SENTENCE = "commands";
	private static final String WHAT = "what";
	private static final String WHAT_SENTENCE = "whats";
	
	
	Context context;
	
	public ActionDbHandler(Context context)
	{
		super(context, DBNAME, null, DBVERSION);
		this.context=context;
	}
	
	@Override
	public void onCreate(SQLiteDatabase db)
	{
		String createTable = "CREATE TABLE " + TABLENAME + "(" + ID + 
								" INTEGER PRIMARY KEY," + COMMAND + " TEXT," 
								+ COMMAND_SENTENCE + " TEXT," + WHAT + " TEXT,"
								+ WHAT_SENTENCE + " TEXT)";
		db.execSQL(createTable);
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
	{
		db.execSQL("DROP TABLE IF EXISTS " + TABLENAME);
		onCreate(db);
	}
	
	public void addAction(Action action)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		
		ContentValues values = new ContentValues();
		values.put(COMMAND, action.getCommand());
		values.put(COMMAND_SENTENCE, action.getCommandS());
		values.put(WHAT, action.getWhat());
		values.put(WHAT_SENTENCE, action.getWhatS());
		
		db.insert(TABLENAME, null, values);
		
		db.close();
	}
	public Action getAction(int id)
	{
		Action action;
		
		SQLiteDatabase db = this.getReadableDatabase();
		try
		{
		String selection = ID + "=" + String.valueOf(id);
		
		Cursor cursor = db.query(TABLENAME, new String[]{ID, COMMAND, COMMAND_SENTENCE, WHAT, WHAT_SENTENCE}
										,selection,null,null,null,null);
		if(cursor!=null) cursor.moveToFirst();
			
				action = new Action( 	Integer.parseInt(cursor.getString(0)),
		
										cursor.getString(1),
										cursor.getString(2),
										cursor.getString(3),
										cursor.getString(4));
			
		cursor.close();
		}
		catch(Exception e)
		{
			action = new Action("###","###","###","###");
		}
		
		db.close();
		
		return action;
	}
	
	public Action getAction(String command)
	{
		Action action;
		SQLiteDatabase db = this.getReadableDatabase();
		try
		{
		    String selection = COMMAND + " LIKE '" + command + "'";
		
			Cursor cursor = db.query(TABLENAME, new String[]{ID, COMMAND, COMMAND_SENTENCE, WHAT, WHAT_SENTENCE}
										,selection,null,null,null,null);
		
	
		if(cursor!=null) cursor.moveToFirst();
					
			action = new Action( 	Integer.parseInt(cursor.getString(0)),
											cursor.getString(1),
											cursor.getString(2),
											cursor.getString(3),
											cursor.getString(4));
			cursor.close();
		}
		catch(Exception e)
		{
			action = new Action("###","###","###","###");
		}
		
		
		db.close();
		
		return action;
	}
	
	public Action getAction(String command, String what)
	{
		Action action;
		SQLiteDatabase db = this.getReadableDatabase();
		try
		{
		    String selection = COMMAND + " LIKE '" + command + "' AND " + WHAT + " LIKE '" + what + "'";
		
			Cursor cursor = db.query(TABLENAME, new String[]{ID, COMMAND, COMMAND_SENTENCE, WHAT, WHAT_SENTENCE}
										,selection,null,null,null,null);
		
	
		if(cursor!=null) cursor.moveToFirst();
					
			action = new Action( 	Integer.parseInt(cursor.getString(0)),
											cursor.getString(1),
											cursor.getString(2),
											cursor.getString(3),
											cursor.getString(4));
			cursor.close();
		}
		catch(Exception e)
		{
			action = new Action("###","###","###","###");
		}
		
		
		db.close();
		
		return action;
	}
	
	public ArrayList<Action> getAllActions()
	{
		ArrayList<Action> list = new ArrayList<Action>();
		
		SQLiteDatabase db = this.getReadableDatabase();
		
		String query = "SELECT * FROM " + TABLENAME;
				
		Cursor cursor = db.rawQuery(query,null);

		if(cursor.moveToFirst())
		{					
			do
			{
				Action action = new Action( 	Integer.parseInt(cursor.getString(0)),
												cursor.getString(1),
												cursor.getString(2),
												cursor.getString(3),
												cursor.getString(4));
				list.add(action);
			}
			while(cursor.moveToNext());
		}
		cursor.close();
		db.close();
				
				
		return list;
	}
	
	public void deleteAction(String command)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLENAME, COMMAND + "=?", new String[]{command});
		db.close();		
	}
	public void deleteAction(String command, String what)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLENAME, COMMAND + "=? AND " +  WHAT + "=?", new String[]{command,what});
		db.close();		
	}

}
