package com.velldrin.smartvoiceassistant;

public class Action {
	
	private int id;
	private String command,commandS;
	private String what, whatS;
	
	public Action(int id, String command, String commandS, String what, String whatS)
	{
		this.id = id;
		this.command=command;
		this.commandS=commandS;
		this.what=what;
		this.whatS=whatS;
	}
	public Action(String command, String commandS, String what, String whatS)
	{
		this.command=command;
		this.commandS=commandS;
		this.what=what;
		this.whatS=whatS;
	}
	
	public void setCommand(String command)
	{
		this.command=command;
	}
	public void setCommandS(String commandS)
	{
		this.commandS=commandS;
	}
	public void setWhat(String what)
	{
		this.what=what;
	}
	public void setWhats(String whatS)
	{
		this.whatS=whatS;
	}
	public int getID()
	{
		return id;
	}
	public String getCommand()
	{
		return command;
	}
	public String getCommandS()
	{
		return commandS;
	}
	public String getWhat()
	{
		return what;
	}
	public String getWhatS()
	{
		return whatS;
	}

}
