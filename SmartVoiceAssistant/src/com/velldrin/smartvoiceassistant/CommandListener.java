package com.velldrin.smartvoiceassistant;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class CommandListener {
	
	Context context;
	
	public CommandListener(Context context)
	{
		this.context=context;
	}
	
	public Action recognizeAction(ArrayList<String> list)
	{

		Action actionr = new Action("###","###","###","###");
		
		ActionDbHandler dbhandler = new ActionDbHandler(context);
		ArrayList<Action> actionList = dbhandler.getAllActions();
		dbhandler.close();
		
		loop:
		for(String recordSentence : list)
		{
			
			for(Action action : actionList)
			{		
						
				String commandS="";
				String whatS="";
				
				if(recordSentence.contains(action.getCommandS()))
				{
					commandS = action.getCommandS();
				}
				if(recordSentence.contains(action.getWhatS()))
				{
					whatS = action.getWhatS();
				}	
										
					if(commandS!="" && whatS!="") {
						if(actionr.getCommandS().length()<commandS.length() || actionr.getWhatS().length()<whatS.length())
						actionr = action;	
					}
					else if(commandS!="" && action.getCommand().equals("navigation")) 
					{
						String target = recordSentence.replace(action.getCommandS(),"");
						actionr = new Action(action.getCommand(),action.getCommandS(),
											"#NAVI#", target);
						break loop;
						
						
					}
					else if(commandS!="" && action.getCommand().equals("search")) 
					{
						String target = recordSentence.replace(action.getCommandS(),"");
						actionr = new Action(action.getCommand(),action.getCommandS(),
											"#SEARCH#", target);
						break loop;

						
					}
					//mozna dopracowac i zrobic tak jak wyzej
					else if(commandS!="" && action.getCommand().equals("dismissGetSMS")) 
					{
						if(VoiceService.mode==2)
						{	
							actionr = new Action(action.getCommand(),action.getCommandS(),
									"#dismissGetSMS#", "#dismissGetSMS#");
							break loop;
						}
					}
					else if(commandS!="" && action.getCommand().equals("readGetSMS")) 
					{
						if(VoiceService.mode==2)
						{							
							actionr = new Action(action.getCommand(),action.getCommandS(),
									"#readGetSMS#", "#readGetSMS#");
							break loop;
						}
					}
					
			}
				
			
			
		}
		
		return actionr;
	}
	
	public void recognizeActionSMS(ArrayList<String> list)
	{
		boolean command=true;
		for(String recordSentence : list)
		{
			ActionDbHandler dbhandler = new ActionDbHandler(context);
	        
			if(recordSentence.contains(dbhandler.getAction("deleteAllSMS").getCommandS()))
			{
				Intent intent = new Intent("SMS");
				intent.putExtra("deleteAllSMS", true);
				context.sendBroadcast(intent);
				command=true;
				break;
			}
			else if(recordSentence.contains(dbhandler.getAction("deleteSMS").getCommandS()))
			{
				Intent intent = new Intent("SMS");
				intent.putExtra("deleteSMS", true);
				context.sendBroadcast(intent);
				command=true;
				break;
			}
			else if(recordSentence.contains(dbhandler.getAction("dismissSMS").getCommandS()))
			{
				Intent intent = new Intent("SMS");
				intent.putExtra("dismissSMS", true);
				context.sendBroadcast(intent);
				command=true;
				break;
			}
			else if(recordSentence.contains(dbhandler.getAction("sendSMS").getCommandS()))
			{
				Intent intent = new Intent("SMS");
				intent.putExtra("sendSMS", true);
				context.sendBroadcast(intent);
				command=true;
				break;
			}
			else 
			{
					command = false;
			}
	
			dbhandler.close();
		}
		
		if(!command)
		{
			Intent intent = new Intent("SMS");
			intent.putExtra("addText", list.get(0));
			context.sendBroadcast(intent);
		}
		else Toast.makeText(context,context.getResources().getString(R.string.recognition_message), Toast.LENGTH_SHORT).show();
		
	}
	
	

}
