package com.velldrin.smartvoiceassistant;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.preference.PreferenceManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.widget.Toast;

public class StatePhoneReceiver extends PhoneStateListener {
    Context context;
    public static boolean callFromApp=false, callFromOffHook=false;
    public StatePhoneReceiver(Context context) {
        this.context = context;
    }

    @Override
    public void onCallStateChanged(int state, String incomingNumber) {
        super.onCallStateChanged(state, incomingNumber);
        
        switch (state) {
        
        case TelephonyManager.CALL_STATE_OFFHOOK: 
        	
        	if(VoiceService.status==1) VoiceService.mode=3;
        	//Call is established
         if (callFromApp) {
             callFromApp=false;
             callFromOffHook=true;
                  
             try {
               Thread.sleep(500); // Delay 0,5 seconds to handle better turning on loudspeaker
             } catch (InterruptedException e) {
             }
          
             //Activate loudspeaker
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
     		if(sharedPreferences.getBoolean("pref_key_loudspeaker", true))
     		{
            
             AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
             audioManager.setMode(AudioManager.MODE_IN_CALL);
             audioManager.setSpeakerphoneOn(true);
             
     		}
             
             
          }
          break;
        
       case TelephonyManager.CALL_STATE_IDLE:
    	   
    	   if(VoiceService.status==1) VoiceService.mode=0;
			
    	   //Call is finished
         if (callFromOffHook) {
               callFromOffHook=false;
               AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
               audioManager.setMode(AudioManager.MODE_NORMAL); //Deactivate loudspeaker
               
              
            }
         break;

        }
    }
}