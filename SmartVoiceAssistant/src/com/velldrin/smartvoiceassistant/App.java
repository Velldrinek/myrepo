package com.velldrin.smartvoiceassistant;

import android.graphics.drawable.Drawable;

class App{
    private String appName;
    private String appKey;
    private String appLink;
    private Drawable icon;
    
    public String getAppName()
    {
    	return appName;
    }   
    public String getAppKey()
    {
    	return appKey;
    }
    public String getAppLink()
    {
    	return appLink;
    }
    public Drawable getAppIcon()
    {
    	return icon;
    }
    
    public void setAppName(String appName)
    {
    	this.appName = appName;
    }
    public void setAppLink(String appLink)
    {
    	this.appLink = appLink;
    }
    public void setAppKey(String appKey)
    {
    	this.appKey = appKey;
    }
    public void setAppIcon(Drawable icon)
    {
    	this.icon = icon;
    }
}

	


