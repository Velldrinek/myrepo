package com.velldrin.smartvoiceassistant;

import android.app.Activity;
import android.app.KeyguardManager;
import android.app.KeyguardManager.KeyguardLock;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.widget.Toast;




public class SettingsActivity extends PreferenceActivity implements OnSharedPreferenceChangeListener {


	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);

		
	}
	
	 public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		 
		     if (key.equals("pref_key_notification")) {
	            if(VoiceService.status==1)
	            {
	              NotificationBuilder notification = new NotificationBuilder(this);
	  		      notification.build(this.getResources().getString(R.string.n_message_service_on));
	  		      notification.run();
	            }
	            else
	            {
	              NotificationBuilder notification = new NotificationBuilder(this);
		  		  notification.build(this.getResources().getString(R.string.n_message_service_off));
		  		  notification.run();
	            }
	        }
	        
	       
	        
	    }
	 
	 @Override
	 protected void onResume() {
	     super.onResume();
	     getPreferenceScreen().getSharedPreferences()
	             .registerOnSharedPreferenceChangeListener(this);
	 }

	 @Override
	 protected void onPause() {
	     super.onPause();
	     getPreferenceScreen().getSharedPreferences()
	             .unregisterOnSharedPreferenceChangeListener(this);
	 }


}
