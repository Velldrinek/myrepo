package com.velldrin.smartvoiceassistant;

import java.util.ArrayList;

import android.app.Activity;
import android.app.KeyguardManager;
import android.app.KeyguardManager.KeyguardLock;
import android.app.SearchManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.provider.ContactsContract.PhoneLookup;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;
import android.widget.Toast;

import android.telephony.PhoneStateListener;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;

public class VoiceService extends Service {
	
	    private SpeechRecognizer mSpeechRecognizer;
	    private Intent mSpeechRecognizerIntent;
	    private final Messenger mServerMessenger = new Messenger(new IncomingHandler(this));

	    private boolean mIsListening;
	    
	    private volatile boolean mIsAlive;

	    static final int MSG_RECOGNIZER_START_LISTENING = 1;
	    static final int MSG_RECOGNIZER_CANCEL = 2;

	    private Context context;
	    private AudioManager mAudioManager ;   
	    
	    //status=0 OFF
	    //status=1 ON
	    public static int status = 0;
	    //mode=0 - commands mode
	    //mode=1 - sms mode
	    //mode=2 - sms get mode
	    //mode=3 - phone
	    public static int mode = 0;
	    
	    private KeyguardManager keyguardManager;
        private KeyguardLock lock;
        
        private StatePhoneReceiver myPhoneStateListener;
  		private TelephonyManager manager;
  		
        private final BroadcastReceiver mReceiver = new BroadcastReceiver()
        {
        	@Override
            public void onReceive(Context context, Intent intent) {
                // TODO Auto-generated method stub
        		PowerManager pm = (PowerManager) context.getSystemService(POWER_SERVICE);
        		WakeLock wl = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ON_AFTER_RELEASE | PowerManager.ACQUIRE_CAUSES_WAKEUP, "TAG");
        		wl.acquire();
        		wl.release();

                if(intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")){
                    Bundle bundle = intent.getExtras();        
                    SmsMessage[] msgs = null;
                    String msg_from = "";
                    String msgBody = "";
                    if (bundle != null){
                        //---retrieve the SMS message received---
                        try{
                            Object[] pdus = (Object[]) bundle.get("pdus");
                            msgs = new SmsMessage[pdus.length];
                            for(int i=0; i<msgs.length; i++){
                                msgs[i] = SmsMessage.createFromPdu((byte[])pdus[i]);
                                msg_from = msgs[i].getDisplayOriginatingAddress();
                                msgBody = msgBody + msgs[i].getMessageBody();
                            }
                        }catch(Exception e){
                        }
                        
                        String contact;
                        
                        try{
                        	Uri uri = Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI, Uri.encode(msg_from));
                        	Cursor cursor = context.getContentResolver().query(uri,new String[]{PhoneLookup.DISPLAY_NAME}, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME );
            	   			cursor.moveToLast();
            	   			contact = cursor.getString(0);
                        }
                        catch(Exception e)
                        {
                        	contact=msg_from;
                        }
                        
        	   			
                        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
                		if(sharedPreferences.getBoolean("pref_key_get_SMS", true))
                		{
                        Intent i = new Intent(context, GetSmsActivity.class);
    	        		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    	        		i.putExtra("Contact", contact);
    	        		i.putExtra("Number", msg_from);
    	        		i.putExtra("Message", msgBody);
    	        		startActivity(i);	
                		}
                    }
                }
            }
        };
	  
        private final BroadcastReceiver SMSReceiver = new BroadcastReceiver() {

			@Override
			public void onReceive(Context context, Intent intent) {
				

				Toast.makeText(context, context.getResources().getString(R.string.sms_sent_message), Toast.LENGTH_SHORT).show();
				
			}
			};

	    @Override
	    public void onCreate()
	    {
	        super.onCreate();
	        context = this;
	        
	        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE); 
	        
	       // Toast.makeText(context, " onCreate", Toast.LENGTH_SHORT).show();
	        boolean available = SpeechRecognizer.isRecognitionAvailable(this);
	        Log.d("Speech", "available = " + available);
	        
	        mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(this);
	        mSpeechRecognizer.setRecognitionListener(new SpeechRecognitionListener());
	        mSpeechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
	        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
	                                         RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
	        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,
	                                         this.getPackageName());
	        
	        resetListener();
	        
			 status=1;
			 // powiadomienie trzeba zrobic w opcjach gdzies ze trzeba wlaczyc powiadomienia innych aplikacji
			 IntentFilter iFilter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
			 registerReceiver(mReceiver, iFilter);
			 
			 registerReceiver(SMSReceiver, new IntentFilter(SmsActivity.SMS_SEND_ACTION));
			 
			 SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
	     		if(sharedPreferences.getBoolean("pref_key_lockscreen", true))
	     		{
		      //preferences w opcjach dodac
				      keyguardManager = (KeyguardManager) context.getSystemService(Activity.KEYGUARD_SERVICE);
				      lock = keyguardManager.newKeyguardLock(KEYGUARD_SERVICE);
		              lock.disableKeyguard();
	     		}
	          NotificationBuilder notification = new NotificationBuilder(context);
		      notification.build(context.getResources().getString(R.string.n_message_service_on));
		      notification.run();
		      
		    myPhoneStateListener = new StatePhoneReceiver(context);
      		manager = ((TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE));
      		
      	    manager.listen(myPhoneStateListener, PhoneStateListener.LISTEN_CALL_STATE); // start listening to the phone changes

	    }
	    
		@Override
		public IBinder onBind(Intent intent) {
			// TODO Auto-generated method stub
			return null;
			
		}
	    
		
		public void resetListener()
		{
			 Message message;
	            try
	            {
	            	message = Message.obtain(null, MSG_RECOGNIZER_CANCEL);
	            	mServerMessenger.send(message);
	            	message = Message.obtain(null, MSG_RECOGNIZER_START_LISTENING);
	                mServerMessenger.send(message);
	            }
	            catch (RemoteException e)
	            {

	            }
		}

	    private static class IncomingHandler extends Handler
	    {
	        private VoiceService mtarget;

	        IncomingHandler(VoiceService target)
	        {
	            mtarget=target;
	        }


	        @Override
	        public void handleMessage(Message msg)
	        {
	          

	            switch (msg.what)
	            {
	                case MSG_RECOGNIZER_START_LISTENING:

	                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
	                    {
	                         //turn off beep sound  
	                         mtarget.mAudioManager.setStreamMute(AudioManager.STREAM_SYSTEM, true);
	                         mtarget.mNoSpeechCountDown.start();
	                    }
	                     if (!mtarget.mIsListening)
	                     {
	                         mtarget.mSpeechRecognizer.startListening(mtarget.mSpeechRecognizerIntent);
	                         mtarget.mIsListening = true;
	                         mtarget.mIsAlive=false;
	                         
	                         
	                        //Log.d(TAG, "message start listening"); //$NON-NLS-1$
	                     }
	                     break;

	                 case MSG_RECOGNIZER_CANCEL:
	                      mtarget.mSpeechRecognizer.cancel();
	                      mtarget.mIsListening = false;
	                      
	                      
	                      //Log.d(TAG, "message canceled recognizer"); //$NON-NLS-1$
	                      break;
	             }
	       } 
	    } 

	    // Count down timer for Jelly Bean work around
	    private CountDownTimer mNoSpeechCountDown = new CountDownTimer(4000, 10)
	    {

	        @Override
	        public void onTick(long millisUntilFinished)
	        {
	            // TODO Auto-generated method stub

	        }

	        @Override
	        public void onFinish()
	        {
	        	if(!mIsAlive)
	            {
	        		resetListener();
	            }
	        	start();
	            
	            
	            
	        }
	    };

	    @Override
	    public void onDestroy()
	    {
	        super.onDestroy();
	        
	        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
            {   
	        	mNoSpeechCountDown.cancel();
            }
	        
	        if (mSpeechRecognizer != null)
	        {
	            mSpeechRecognizer.destroy();
	        }
	        
		      status=0;
		      unregisterReceiver(mReceiver);
		      unregisterReceiver(SMSReceiver);
		      
		      SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		      if(sharedPreferences.getBoolean("pref_key_lockscreen", true) || lock!=null)
	     		{
		    	  lock.reenableKeyguard();
	     		}
		      
		      manager.listen(myPhoneStateListener, PhoneStateListener.LISTEN_NONE);
		      
	          NotificationBuilder notification = new NotificationBuilder(context);
		      notification.build(context.getResources().getString(R.string.n_message_service_off));
		      notification.run();
		      
		      mAudioManager.setStreamMute(AudioManager.STREAM_SYSTEM, false);
		     
	    }

	    private class SpeechRecognitionListener implements RecognitionListener
	    {

	        @Override
	        public void onBeginningOfSpeech()
	        {
	            // speech input will be processed, so there is no need for count down anymore
	            mIsAlive=true;
	            Log.d("voice service", "onBeginingOfSpeech"); //$NON-NLS-1$
	            //Toast.makeText(context, "on beginning", Toast.LENGTH_SHORT).show();
	        }

	        @Override
	        public void onBufferReceived(byte[] buffer)
	        {

	        }

	        @Override
	        public void onEndOfSpeech()
	        {
	            Log.d("voice service", "onEndOfSpeech"); //$NON-NLS-1$

	           // Toast.makeText(context, " onEndOfSpeech", Toast.LENGTH_SHORT).show();
	         }

	        @Override
	        public void onError(int error)
	        {
	            //Toast.makeText(context, " onError", Toast.LENGTH_SHORT).show();
	            resetListener();
	            Log.d("voice service", "error = " + error); //$NON-NLS-1$
	        }

	        @Override
	        public void onEvent(int eventType, Bundle params)
	        {
	        	Log.d("voice service", "onEvent");
	        }

	        @Override
	        public void onPartialResults(Bundle partialResults)
	        {
	             //Toast.makeText(context, " onPartialResults", Toast.LENGTH_SHORT).show();
	        }

	        @Override
	        public void onReadyForSpeech(Bundle params)
	        {
	        	 // Toast.makeText(context, " onReadyForSpeech", Toast.LENGTH_SHORT).show();
	        	  
	            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
	            {
	            	mAudioManager.setStreamMute(AudioManager.STREAM_SYSTEM, false);
	            }
	            
	            Log.d("voice service", "onReadyForSpeech"); //$NON-NLS-1$
	        }

	        @Override
	        public void onResults(Bundle results)
	        {
	        	
	        	ArrayList<String> list = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
	        	CommandListener cListener = new CommandListener(context);
	        	
	        	if(mode==0 || mode==2)
	        	{
	        		
			        	Action action = cListener.recognizeAction(list);
				        	
			        	if(!action.getCommand().equals("###"))
				        	{
				        		PowerManager pm = (PowerManager) context.getSystemService(POWER_SERVICE);
				        		WakeLock wl = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ON_AFTER_RELEASE | PowerManager.ACQUIRE_CAUSES_WAKEUP, "TAG");
				        		wl.acquire();
				        		wl.release();
			     		
				        		executeCommand(action);
				        		Toast.makeText(context,context.getResources().getString(R.string.recognition_message), Toast.LENGTH_SHORT).show();
				        		Log.d("voice service", "rozpoznano" + action.getCommand()); 
			
				        		
				        	}
			        	else Log.d("voice service", "nie rozpoznano"); 
	        	}
	        	else if(mode==1)
	        	{
	        		
	        		
	        		PowerManager pm = (PowerManager) context.getSystemService(POWER_SERVICE);
	        		WakeLock wl = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ON_AFTER_RELEASE | PowerManager.ACQUIRE_CAUSES_WAKEUP, "TAG");
	        		wl.acquire();
	        		wl.release();
	        		
	        		cListener.recognizeActionSMS(list);
	        	}
	        	      	  
	         	  
	        	resetListener();
	        	Log.d("voice service", "onResults");

	        }

	        @Override
	        public void onRmsChanged(float rmsdB)
	        {

	        }

	        
	        private void executeCommand(Action action)
	        {
        		
	        	if(action.getCommand().equals("writeSMS"))
	        	{
	        		 String contact;
                     
                     try{
                     	Uri uri = Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI, Uri.encode(action.getWhat()));
                     	Cursor cursor = context.getContentResolver().query(uri,new String[]{PhoneLookup.DISPLAY_NAME}, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME );
         	   			cursor.moveToLast();
         	   			contact = cursor.getString(0);
                     }
                     catch(Exception e)
                     {
                     	contact=action.getWhatS();
                     }
	        		
	        		
	        		Intent intent = new Intent(context, SmsActivity.class);
	        		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	        		intent.putExtra("Contact", contact);
	        		intent.putExtra("Number", action.getWhat());
	        		startActivity(intent);	        		
	        	}
	        	else if(action.getCommand().equals("phone"))
	        	{
	        		String phoneNumber = action.getWhat();
	        		
	        	    StatePhoneReceiver.callFromApp=true;
	        	    Intent intent = new Intent(android.content.Intent.ACTION_CALL,
	        	                          Uri.parse("tel:+" + phoneNumber)); // Make the call 
	        	    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	        	    startActivity(intent);   
	        	}
	        	else if(action.getCommand().equals("navigation"))
	        	{	    
	        		if(!action.getWhatS().equals(""))
	        		{
		                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("google.navigation:q= " + action.getWhatS())); 
		                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		                startActivity(intent);
	        		}
	        	}
	        	else if(action.getCommand().equals("search"))
	        	{
	        		if(!action.getWhatS().equals(""))
	        		{	
	        			Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
	        			intent.putExtra(SearchManager.QUERY, action.getWhatS());
	        			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	        			startActivity(intent);
	        		}
	        	}
	        	else if(action.getCommand().equals("app"))
	        	{
	        			PackageManager manager = getPackageManager();
	        		    Intent intent = manager.getLaunchIntentForPackage(action.getWhat());
	        		    intent.addCategory(Intent.CATEGORY_LAUNCHER);
	        			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	        		    startActivity(intent);
	        	}
	        	else if(action.getCommand().equals("dismissGetSMS"))
	        	{
		        		 Intent intent = new Intent("GetSMS");
						 intent.putExtra("dismissGetSMS", true);
						 context.sendBroadcast(intent);
	        	}
	        	else if(action.getCommand().equals("readGetSMS"))
	        	{
	        			//musi byc zainstalowane text-to-speech trzeba dac powiadomienie
		        		 Intent intent = new Intent("GetSMS");
						 intent.putExtra("readGetSMS", true);
						 context.sendBroadcast(intent);
	        	}
	        

	        }
	                
	    }

	
	
}
