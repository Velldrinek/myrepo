package com.velldrin.smartvoiceassistant;

public class Contact
{
	private String name;
	private String number;
	private String key;
	
	public Contact(String name, String number)
	{
		this.name=name;
		this.number=number;
	}
	
	public Contact(String name, String number, String key)
	{
		this.name=name;
		this.number=number;
		this.key=key;
	}
	
	public String getName()
	{
		return name;
	}
	public String getNumber()
	{
		return number;
	}
	public String getKey()
	{
		return key;
	}
	public void setKey(String key)
	{
		this.key=key;
	}
}