package com.velldrin.smartvoiceassistant;

import java.util.ArrayList;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.speech.RecognizerIntent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.widget.Toast;

public class MainActivity extends SherlockFragmentActivity {
	
    private ActionBar actionbar;
    
    
   // public Context context = this;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		actionbar = getSupportActionBar();
      
        //context=this;
        
        FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
        tx.replace(R.id.main,Fragment.instantiate(MainActivity.this, "com.velldrin.smartvoiceassistant.FragmentActions"));
        tx.commit(); 

        //ActionDbHandler dbhandler = new ActionDbHandler(this);
        //dbhandler.addAction(new Action("navigation","nawigacja","#NAVI#","lol"));
        //dbhandler.close();
        
        
	}

	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
         
		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.main, menu);
		
		return true;
    }
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch(item.getItemId())
		{
			case R.id.settings:
			{
				Intent intent = new Intent(this, SettingsActivity.class);
				startActivity(intent);						
				return true;
			}
			
		}
		return super.onOptionsItemSelected(item);
	}
	
	 
	 
	
	

}


