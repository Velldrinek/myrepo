package com.velldrin.smartvoiceassistant;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;


public class NotificationBuilder {
	
	
	private static final String KEY_BUTTON_ONE = "com.velldrin.smartvoiceassistant.buttonone";
	private static final String KEY_BUTTON_TWO = "com.velldrin.smartvoiceassistant.buttontwo";
	private static final String KEY_BUTTON_THREE = "com.velldrin.smartvoiceassistant.buttonthree";

	private NotificationManager mNotificationManager;
	private NotificationCompat.Builder mBuilder;
	Context context;
	

	public NotificationBuilder(Context context)
	{
		this.context=context;
		
	}
	
	public void build(String message)
	{
		mBuilder = new NotificationCompat.Builder(context);

	    mBuilder.setContentTitle(context.getResources().getString(R.string.notification_title));
	  
	    mBuilder.setSmallIcon(R.drawable.on_notification);
	    
	    if(!message.equals("")) mBuilder.setTicker(message);
	    
	    
	    Intent mIntent;
	    
	    if (android.os.Build.VERSION.SDK_INT>=16)
	    {
	    	 NotificationCompat.InboxStyle bigStyle = new NotificationCompat.InboxStyle();
	    	 bigStyle.setBigContentTitle(context.getResources().getString(R.string.notification_title));
	    	 
		 	 bigStyle.addLine(context.getResources().getString(R.string.n_message));
		 	 if(VoiceService.status==1)
		 	 {
		 	 mBuilder.setContentText(context.getResources().getString(R.string.n_message_service_on));
		 	 bigStyle.addLine(context.getResources().getString(R.string.n_message_service_on));
		 	 }
		 	 else
		 	 {
		 	 mBuilder.setContentText(context.getResources().getString(R.string.n_message_service_off));
		 	 bigStyle.addLine(context.getResources().getString(R.string.n_message_service_off));
		 	 }
	    	
	    	Intent keyIntentOne = new Intent(KEY_BUTTON_ONE);
	 	    PendingIntent pendingKetIntentOne = PendingIntent.getBroadcast(context, 0, keyIntentOne, 0);
	 	    mBuilder.addAction(R.drawable.on_notification,context.getResources().getString(R.string.n_on_btn), pendingKetIntentOne);
	 		 
	 	    
	 	    Intent keyIntentTwo = new Intent(KEY_BUTTON_TWO);
	 	    PendingIntent pendingKetIntentTwo = PendingIntent.getBroadcast(context, 0, keyIntentTwo, 0);
	 	    mBuilder.addAction(R.drawable.off_notification,context.getResources().getString(R.string.n_off_btn), pendingKetIntentTwo);
	 	    
	 	    Intent keyIntentThree = new Intent(KEY_BUTTON_THREE);
	 	    PendingIntent pendingKetIntentThree = PendingIntent.getBroadcast(context, 0, keyIntentThree, 0);
	 	    mBuilder.addAction(R.drawable.settings_notification,context.getResources().getString(R.string.n_settings_btn), pendingKetIntentThree);
	 	    
	 	    mBuilder.setStyle(bigStyle);
	 	    
		    mIntent = new Intent(context, MainActivity.class);
		   
	    }
	    else 
	    {
	    	 if(VoiceService.status==1)
		 	 {
		 	 mBuilder.setContentText(context.getResources().getString(R.string.n_message_service_on));
		 	 }
		 	 else
		 	 {
		 	 mBuilder.setContentText(context.getResources().getString(R.string.n_message_service_off));
		 	 }

	    	mIntent = new Intent(context, MainActivity.class);
		    
	    }
	    PendingIntent mPendingIntent = PendingIntent.getActivity(context, 0, mIntent, 0);
	    
	    mBuilder.setContentIntent(mPendingIntent);
	    //mBuilder.setOngoing(true);
	    
	    mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE); 
		
	}
	public void run()
	{
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		if(sharedPreferences.getBoolean("pref_key_notification", true))
		{
		mNotificationManager.notify(0, mBuilder.build());
		}
		else cancel();
	}
	public void cancel()
	{
		mNotificationManager.cancel(0);
	}

}
