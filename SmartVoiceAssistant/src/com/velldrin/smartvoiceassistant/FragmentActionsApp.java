package com.velldrin.smartvoiceassistant;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.actionbarsherlock.app.SherlockFragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.speech.RecognizerIntent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class FragmentActionsApp extends SherlockFragment implements OnClickListener{
	
	private static final int VOICE_RECOGNITION_TURN_ON_APP_CODE = 9661;
	
	private Button turnOnBtn,
				   confirmBtn,
				   cancelBtn;
	
	private ListView lv;
	
	private static boolean state = false;
	
	private ProgressDialog mProgressDialog;
	
	private ArrayList<App> appList;
	
	 public static Fragment newInstance(Context context) {
		 FragmentActionsApp f = new FragmentActionsApp();
		 
	        return f;
	    }
	 
	    @Override
	    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
	        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_actions_app, null);
	        
	       if (container == null) {
	            
	            return null;
	        }
	       
	       lv = (ListView) root.findViewById(R.id.app_list);
	       setAppList();
	       turnOnBtn = (Button) root.findViewById(R.id.turn_on_btn);
	       confirmBtn = (Button) root.findViewById(R.id.confirm_btn);
	       cancelBtn = (Button) root.findViewById(R.id.cancel_btn);
	       
	       turnOnBtn.setOnClickListener(this);
	       confirmBtn.setOnClickListener(this);
	       cancelBtn.setOnClickListener(this);
	        
	       ActionDbHandler dbhandler = new ActionDbHandler(getActivity());
	       
		   if(!dbhandler.getAction("app").getCommandS().equals("###"))
		   {
			   turnOnBtn.setText(dbhandler.getAction("app").getCommandS());
		   }

	       dbhandler.close();
       
	       return root;
	    }

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Fragment f = Fragment.instantiate(getActivity(), "com.velldrin.smartvoiceassistant.FragmentActions");
			FragmentTransaction tx = getActivity().getSupportFragmentManager().beginTransaction();
			
			switch(v.getId())
			{
			case R.id.turn_on_btn:
				startVoiceRecognitionActivity(VOICE_RECOGNITION_TURN_ON_APP_CODE);
			break;
			case R.id.confirm_btn:
				confirmChanges();
			break;		
			case R.id.cancel_btn:
				tx.replace(R.id.main, f);
		        tx.commit();
			break;
			}
			
		}
		
		
		private void startVoiceRecognitionActivity(int code) {
			    
				if(VoiceService.status==1)
					{
						getActivity().stopService(new Intent(getActivity(), VoiceService.class));
						state=true;
					}
				
				        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
			
				        // Specify the calling package to identify your application
				        intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, getClass().getPackage().getName());
			 
				        // Display an hint to the user about what he should say.
				        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, getActivity().getResources().getString(R.string.dialog_message_recognition));
			
				        // Given an hint to the recognizer about what the user is going to say
				        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
				                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
			
				        // Specify how many results you want to receive. The results will be sorted
				        // where the first result is the one with higher confidence.
				        intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1);
				        startActivityForResult(intent, code);
				        
				        
				        
				    }
		
		@Override
		 public void onActivityResult(int requestCode, int resultCode, Intent data) {
			 
			   
		            super.onActivityResult( requestCode, resultCode, data );
		            		        
		            
		            if(data!=null)
		            {
			            ArrayList<String> matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
			            String word = matches.get(0);          
			           
			            if(requestCode == VOICE_RECOGNITION_TURN_ON_APP_CODE)
			            {
			            	turnOnBtn.setText(word);
			            }
			            
			            else
				        {
				        	//zabezpieczenie przed podwojeniem
				        	for(App app : appList)
				        	{
				        		if(app.getAppKey().equals(word))
				        		{
				        			word = appList.get(requestCode).getAppKey();
				        			Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.toast_message_in_use), Toast.LENGTH_SHORT).show();
				        		}
				        	}
				        	appList.get(requestCode).setAppKey(word);
				        	setListView(appList);
				        }
		            }

		            
					if(state==true)
					{
						getActivity().startService(new Intent(getActivity(), VoiceService.class));
						state=false;
					}
		 	    }

		
		public void setAppList()
		{
			 mProgressDialog = new ProgressDialog(getActivity());
			 mProgressDialog.setCancelable(false);
			 mProgressDialog.setCanceledOnTouchOutside(false);
			 mProgressDialog.setTitle(getActivity().getResources().getString(R.string.fragment_dialog_loading_title));
			 mProgressDialog.setMessage(getActivity().getResources().getString(R.string.fragment_dialog_loading_apps));
	     	 mProgressDialog.setIndeterminate(false);
	     	 mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			
			new AsyncTask<Void, Void, Void>(){
	            @Override
	            protected void onPreExecute(){
	                mProgressDialog.show();
	            }
	            
		        @Override
		        protected Void doInBackground(Void... arg0) {
		        	
		        	doAppList();
		     	   
		            return null;
		        }
		           @Override
		            protected void onPostExecute(Void result) {
		        	   
		        	   
		        	    setListView(appList);
		        	    try{
		        	    mProgressDialog.dismiss();
		        	    }
		        	    catch(Exception e){}
		           }
		    }.execute();
			
			 	
		}
		
		private void doAppList()
		{
			
			try{
        		appList = getInstalledApps();
	        	
        	}
        	catch(Exception e)
        	{
        		
        	}
		}
		
		private void setListView(ArrayList<App> appList)
		{
			try{
				CustomAppsAdapter adapter = new CustomAppsAdapter(getActivity(), appList);
				lv.setAdapter(adapter);
				
				lv.setOnItemClickListener(new OnItemClickListener() {
			        @Override
					public void onItemClick(AdapterView<?> parent, View view,
			                int position, long id) {
			
			        	startVoiceRecognitionActivity(position);
			     
			        }
			        
			   
			    });
				
				
				int height=0;
				for (int i = 0; i < lv.getAdapter().getCount(); i++) {
				          View listItem = lv.getAdapter().getView(i, null, lv);
				          listItem.measure(0, 0);
				          
				          if(i<2) height += listItem.getMeasuredHeight();
				          height += listItem.getMeasuredHeight();
				}
				
				ViewGroup.LayoutParams parameters = lv.getLayoutParams();
				parameters.height = height;
				lv.setLayoutParams(parameters);
			}
			catch(Exception e)
			{
				
			}
         
		}
		
		public void confirmChanges()
		{
			 mProgressDialog = new ProgressDialog(getActivity());
			 mProgressDialog.setCancelable(false);
			 mProgressDialog.setCanceledOnTouchOutside(false);
			 mProgressDialog.setTitle(getActivity().getResources().getString(R.string.fragment_dialog_saving_title));
			 mProgressDialog.setMessage(getActivity().getResources().getString(R.string.fragment_dialog_saving_text));
	     	 mProgressDialog.setIndeterminate(false);
	     	 mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			
			new AsyncTask<Void, Void, Void>(){
	            @Override
	            protected void onPreExecute(){
	                mProgressDialog.show();
	            }
	            
		        @Override
		        protected Void doInBackground(Void... arg0) {
		        	
		        	Action action;
					ActionDbHandler dbhandler = new ActionDbHandler(getActivity());
					dbhandler.deleteAction("app");
					
					for(App app : appList)
					{
						action = new Action("app",turnOnBtn.getText().toString(),app.getAppLink(), app.getAppKey());
			            dbhandler.addAction(action);   
			            
					}
					
		            dbhandler.close();
		     	   
		            return null;
		        }
		           @Override
		            protected void onPostExecute(Void result) {
		        	   
		        	   try{
			        	    mProgressDialog.dismiss();
			        	    
			    			Fragment f = Fragment.instantiate(getActivity(), "com.velldrin.smartvoiceassistant.FragmentActions");
			    			FragmentTransaction tx = getActivity().getSupportFragmentManager().beginTransaction();
			    			
			    			tx.replace(R.id.main, f);
			    	        tx.commit();
		        	   }
		        	   catch(Exception e)
		        	   {
		        		   
		        	   }
		        	 }
		    }.execute();
			
			 	
		}
		
		
		private ArrayList<App> getInstalledApps()
		{
			ArrayList<App> res = new ArrayList<App>();
			PackageManager packageManager = getActivity().getPackageManager();
		    List<ApplicationInfo> apps = packageManager.getInstalledApplications(PackageManager.PERMISSION_GRANTED);
		    Collections.sort(apps, new ApplicationInfo.DisplayNameComparator(packageManager));
		    
		    for(int i=0;i<apps.size();i++)
		    {
		    	if(getActivity().getPackageManager().getLaunchIntentForPackage(apps.get(i).packageName) == null)
		    	{
		    		continue;
		        }
		    	
		    	ApplicationInfo a = apps.get(i);

		    	  	App app = new App();
			        app.setAppName(a.loadLabel(getActivity().getPackageManager()).toString());
			        app.setAppLink(a.packageName);
			        app.setAppIcon(a.loadIcon(getActivity().getPackageManager()));
			        
			        	ActionDbHandler dbhandler = new ActionDbHandler(getActivity());
			            Action action = dbhandler.getAction("app", app.getAppLink());
			            dbhandler.close();
			            
			            if(action.getCommand().equals("###"))
			            {
			            	app.setAppKey(app.getAppName().toLowerCase());
			            }
			            else
			            {		            	
				            app.setAppKey(action.getWhatS());
			            }
			            
			            
			        
			        res.add(app);
		    }
		    
		    return res;
		}
}
