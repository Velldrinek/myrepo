package com.velldrin.smartvoiceassistant;

import java.util.Locale;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.Menu;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class GetSmsActivity extends Activity implements TextToSpeech.OnInitListener{
	
	private TextView contact,
					 number;
	private EditText message;
	private ScrollView smsView;
	
	private int height;
	private int a = 1;
	
	private TextToSpeech tts;
	
	private final BroadcastReceiver mReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
				
			if(intent.getBooleanExtra("dismissGetSMS", false))
			{
				finish();
			}
			
			if(intent.getBooleanExtra("readGetSMS", false))
			{
				speakOut();
			}
			
			
				
		}
		};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_get_sms);
		
		String contactS = getIntent().getStringExtra("Contact");
		String numberS = getIntent().getStringExtra("Number");
		String messageS = getIntent().getStringExtra("Message");
		
		contact = (TextView) findViewById(R.id.ContactId);
		number = (TextView) findViewById(R.id.NumberId);
		message = (EditText) findViewById(R.id.smsText);
		smsView = (ScrollView) findViewById(R.id.scrollView);
		
		contact.setText(contactS);
		number.setText(numberS);
		message.setText(messageS);
		
		tts = new TextToSpeech(this, this);
		
		
		new CountDownTimer(1000 , 20) {          

			 public void onTick(long millisUntilFinished) {             

			   smsView.scrollBy(0, a);
			   if(smsView.getScrollY()==height)
			   {
				   if(a==1) a=-1;
				   else a=1;
			   }
			   height = smsView.getScrollY();
			   
			 }          

			 public void onFinish() {  
				 start();
			 }      

			}.start(); 
		
		
	}

	
	@Override
	public void onResume()
	{
		super.onResume();
		VoiceService.mode=2;
		 
		registerReceiver(mReceiver, new IntentFilter("GetSMS"));
	}
	@Override
	public void onPause()
	{
		super.onPause();
		VoiceService.mode=0;
		unregisterReceiver(mReceiver);
		
		if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
		
		finish();
		
	}

	@Override
	public void onInit(int status) {
		// TODO Auto-generated method stub
		
		if (status == TextToSpeech.SUCCESS) {
			 
            int result = tts.setLanguage(Locale.getDefault());
 
            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                //Log.e("TTS", "This Language is not supported");
                Toast.makeText(this,getResources().getString(R.string.toast_tts_lang_not_supported), Toast.LENGTH_SHORT).show();
            } else {
            	
            	SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        		if(sharedPreferences.getBoolean("pref_key_auto_read", false))
        		{
        			speakOut();
        		}
            }
 
        } else {
        	Toast.makeText(this, getResources().getString(R.string.toast_tts_not_installed), Toast.LENGTH_SHORT).show();
            Log.e("TTS", "Initilization Failed!");
        }
		
	}
	
    private void speakOut() {
    	 
    	tts.setSpeechRate(0.2f);
        tts.speak(message.getText().toString(), TextToSpeech.QUEUE_FLUSH, null);
    }

}
