package com.velldrin.smartvoiceassistant;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


public class CustomContactsAdapter extends ArrayAdapter<Contact> {
	  private final Context context;
	  private Contact[] contact;
	  
	  public CustomContactsAdapter(Context context, ArrayList<Contact> contact) {
	    super(context, R.layout.row, contact);
	    this.context = context;
	    
	    this.contact = new Contact[contact.size()];
	    this.contact = contact.toArray(this.contact);
	    
	    
	    

	  }

	  @Override
	  public View getView(int position, View convertView, ViewGroup parent) {
	    LayoutInflater inflater = (LayoutInflater) context
	        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    View rowView = inflater.inflate(R.layout.row, parent, false);
	    
	    TextView nameTxt = (TextView) rowView.findViewById(R.id.name_contact);
	    TextView numberTxt = (TextView) rowView.findViewById(R.id.number_contact);
	    TextView keyTxt = (TextView) rowView.findViewById(R.id.key_contact);
	    
	    nameTxt.setText(this.contact[position].getName());
	    numberTxt.setText(this.contact[position].getNumber());
	    keyTxt.setText(this.contact[position].getKey());
	    
	    return rowView;
	  }
}
